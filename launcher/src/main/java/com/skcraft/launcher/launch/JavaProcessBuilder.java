/*
 * SK's Minecraft Launcher
 * Copyright (C) 2010-2014 Albert Pham <http://www.sk89q.com> and contributors
 * Please see LICENSE.txt for license information.
 */

package com.skcraft.launcher.launch;

import static com.skcraft.launcher.util.SharedLocale.tr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;

import com.skcraft.launcher.LauncherException;
import com.skcraft.launcher.launch.JavaRuntimeFinder.JavaRuntimeEntry;
import com.skcraft.launcher.swing.SwingHelper;
import com.skcraft.launcher.util.Environment;
import com.skcraft.launcher.util.Platform;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * A tool to build the entire command line used to launch a Java process.
 * It combines flags, memory settings, arguments, the class path, and
 * the main class.
 */
@ToString
public class JavaProcessBuilder {

    private static final Pattern argsPattern = Pattern.compile("(?:([^\"]\\S*)|\"(.+?)\")\\s*");

    @Getter @Setter private int minMemory;
    @Getter @Setter private int maxMemory;
    @Getter @Setter private int permGen;

    @Getter private final JavaRuntimeEntry javaRuntime;
    @Getter private final List<File> classPath = new ArrayList<File>();
    @Getter private final List<String> flags = new ArrayList<String>();
    @Getter private final List<String> args = new ArrayList<String>();
    @Getter @Setter private String mainClass;
    
    public JavaProcessBuilder() {
    	javaRuntime = JavaRuntimeFinder.findBestJRE();
    }
    
    public JavaProcessBuilder(@Nullable String javaRuntimePath) throws IOException {
    	this(javaRuntimePath == null || javaRuntimePath.trim().isEmpty() ? null : new File(javaRuntimePath));
    }

    public JavaProcessBuilder(@Nullable File javaRuntimePath) throws IOException {
    	if (javaRuntimePath != null) {
            if (!javaRuntimePath.exists()) {
                throw new IOException("The configured Java runtime path '" + javaRuntimePath + "' doesn't exist.");
            }
            
            javaRuntime = new JavaRuntimeEntry(javaRuntimePath);
    	} else {
    		javaRuntime = JavaRuntimeFinder.findBestJRE();
    	}
	}

    public JavaProcessBuilder classPath(File file) {
        getClassPath().add(file);
        return this;
    }

    public JavaProcessBuilder classPath(String path) {
        getClassPath().add(new File(path));
        return this;
    }

    public String buildClassPath() {
        StringBuilder builder = new StringBuilder();
        boolean first = true;

        for (File file : classPath) {
            if (first) {
                first = false;
            } else {
                builder.append(File.pathSeparator);
            }

            builder.append(file.getAbsolutePath());
        }

        return builder.toString();
    }
    
    public void checkEnvironmentAndArgs() throws LauncherException {
    	if (javaRuntime != null) {
	        if (javaRuntime.getVersion()[1] > 8) {
	        	Runner.visibleLoggerWarning(tr("runner.java9"));
	        }
	        
	        if (!javaRuntime.is64Bit() && Environment.getInstance().is64bit() && Environment.getInstance().getPlatform() == Platform.WINDOWS) {
	        	Runner.visibleLoggerWarning(tr("runner.java32bitOn64bitOS"));
	        }
    	}
        

        if (minMemory > 0) {
	    	if (javaRuntime != null && !javaRuntime.is64Bit() && minMemory > 2048 && Environment.getInstance().getPlatform() == Platform.WINDOWS) {
	    		if (Environment.getInstance().is64bit()) {
	    			SwingHelper.openURL("https://forum.toastynetworks.net/faq/the-launcher-does-not-work-how-can-i-fix-my-issue.103/", null);
	    			throw new LauncherException("Your minimum memory setting is above 2048MiB while you are not using Java 64-bit. Please install Java 64 bit.", tr("runner.minimumMemory64bit"));
	    		} else {
	    			throw new LauncherException("Your minimum memory setting is above 2048MiB while you are not using a 64-bit operating system. Please lower the setting to 2048 MiB or less.", tr("runner.minimumMemory32bit"));
	    		}
	    	}
	    }
        
        if (maxMemory > 0) {
        	if (javaRuntime != null && !javaRuntime.is64Bit() && maxMemory >= 2048 && Environment.getInstance().getPlatform() == Platform.WINDOWS) {
        		if (Environment.getInstance().is64bit()) {
        			throw new LauncherException("Your maximum memory setting is above 2048MiB while you are not using Java 64-bit. Please install Java 64 bit.", tr("runner.maximumMemory64bit"));
        		} else {
        			throw new LauncherException("Your maximum memory setting is above 2048MiB while you are not using a 64-bit operating system. Please lower the setting to 2048 MiB or less.", tr("runner.maximumMemory32bit"));
        		}
        	}
        	
        	if (Environment.getInstance().getRamSize() != -1 && Environment.getInstance().getRamSize() <= Integer.MAX_VALUE) {
        		Runner.visibleLoggerWarning("> Your computer has only 2 GiB or less of RAM! This may prevent any modpack from working correctly!");
        	}
        	
        	if (maxMemory <= 2048) {
        		Runner.visibleLoggerWarning("> Your maximum memory setting is 2048 MiB or less! This may prevent some modpack to work fine!");
        	}
        }
    }

    public List<String> buildCommand() {
        List<String> command = new ArrayList<String>();

        if (javaRuntime != null) {
            command.add(javaRuntime.getDir() + File.separator + "java");
        } else {
            command.add("java");
        }

        for (String flag : flags) {
            command.add(flag);
        }
        
        if (minMemory > 0) {
            command.add("-Xms" + String.valueOf(minMemory) + "M");
        }

        if (maxMemory > 0) {
            command.add("-Xmx" + String.valueOf(maxMemory) + "M");
        }

        if (permGen > 0) {
            command.add("-XX:MaxPermSize=" + String.valueOf(permGen) + "M");
        }

        command.add("-cp");
        command.add(buildClassPath());

        command.add(mainClass);

        for (String arg : args) {
            command.add(arg);
        }

        return command;
    }

    /**
     * Split the given string as simple command line arguments.
     *
     * <p>This is not to be used for security purposes.</p>
     *
     * @param str the string
     * @return the split args
     */
    public static List<String> splitArgs(String str) {
        Matcher matcher = argsPattern.matcher(str);
        List<String> parts = new ArrayList<String>();
        while (matcher.find()) {
            parts.add(matcher.group(1));
        }
        return parts;
    }

}
