/*
 * SK's Minecraft Launcher
 * Copyright (C) 2010-2014 Albert Pham <http://www.sk89q.com> and contributors
 * Please see LICENSE.txt for license information.
 */

package com.skcraft.launcher.launch;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.skcraft.launcher.util.Environment;
import com.skcraft.launcher.util.Platform;
import com.skcraft.launcher.util.WinRegistry;

import lombok.Getter;
import lombok.extern.java.Log;

/**
 * Finds the best Java runtime to use.
 */
@Log
public final class JavaRuntimeFinder {

	private JavaRuntimeFinder() {
	}

	/**
	 * Return the path to the best found JVM location.
	 *
	 * @return the JVM location, or null
	 */
	public static JavaRuntimeEntry findBestJRE() {
		if (Environment.getInstance().getPlatform() != Platform.WINDOWS) {
			log.warning("Can't search for JVM path on Platform: "+Environment.getInstance().getPlatform());
			return null;
		}

		final List<JavaRuntimeEntry> entries = new ArrayList<JavaRuntimeEntry>();
		getEntriesFromDefaultPaths(entries);
		
		getEntriesFromRegistry(entries, "SOFTWARE\\JavaSoft\\Java Runtime Environment");
		getEntriesFromRegistry(entries, "SOFTWARE\\JavaSoft\\Java Development Kit");
		
		if (entries.isEmpty()) {
			log.warning("JVM path couldn't be detected!");
			return null;
		}
		
		log.info("JVM path List:");
		for (JavaRuntimeEntry jre : entries) {
			log.info(jre.toString());
		}
		
		JavaRuntimeEntry jre = Collections.max(entries);
		log.info("Selected JVM path: "+jre);
		
		return jre;
	}

	private static void getEntriesFromRegistry(List<JavaRuntimeEntry> entries, String basePath) {
		try {
			List<String> subKeys = WinRegistry.readStringSubKeys(WinRegistry.HKEY_LOCAL_MACHINE, basePath);
			for (String subKey : subKeys) {
				try {
					JavaRuntimeEntry entry = getEntryFromRegistry(basePath, subKey);
					if (entry != null) {
						entries.add(entry);
					}
				} catch (Throwable e) {
					
				}
			}
		} catch (Throwable e) {
			
		}
	}

	private static JavaRuntimeEntry getEntryFromRegistry(String basePath, String version)  throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		String path = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, basePath + "\\" + version, "JavaHome");
		File dir = new File(path);
		if (dir.exists() && new File(dir, "bin" + File.separator + "java.exe").exists()) {
			return new JavaRuntimeEntry(dir, version);
		} else {
			return null;
		}
	}
	
	private static void getEntriesFromDefaultPaths(List<JavaRuntimeEntry> entries) {
		getEntriesFromPath(new File("C:\\Program Files\\Java\\"), entries, true);
	}
	
	private static void getEntriesFromPath(File path, List<JavaRuntimeEntry> entries, boolean is64Bit) {
		if (path.isDirectory()) {
			for (File dir : path.listFiles(JREDirectoryFilter.INSTANCE)) {
				if (new File(dir, "bin" + File.separator + "java.exe").exists()) {
					try {
						entries.add(new JavaRuntimeEntry(dir));
					} catch (Throwable e) {
						
					}
				}
			}
		}
	}
	
	private static class JREDirectoryFilter implements FileFilter {
		private final static JREDirectoryFilter INSTANCE = new JREDirectoryFilter();
		
		@Override
		public boolean accept(File file) {
			return file.isDirectory() && (file.getName().startsWith("jre") || file.getName().startsWith("jdk"));
		}
	}

	public static class JavaRuntimeEntry implements Comparable<JavaRuntimeEntry> {
		@Getter private final File dir;
		@Getter private final int[] version = new int[4];
		@Getter private final boolean is64Bit;
		
		public JavaRuntimeEntry(File dir) {
			this(dir, guessIf64Bit(dir));
		}
		
		public JavaRuntimeEntry(File dir, boolean is64Bit) {
			this(dir, getVersionString(dir), is64Bit);
		}
		
		public JavaRuntimeEntry(File dir, String versionString) {
			this(dir, versionString, guessIf64Bit(dir));
		}
		
		public JavaRuntimeEntry(File dir, String versionString, boolean is64Bit) {
			if (versionString.startsWith("1.")) {
				versionString = versionString.substring(2);
			}
			
			if (dir.getName().equals("java") || dir.getName().equals("java.exe")) {
				this.dir = dir.getParentFile();
			} else if (dir.getName().equals("bin")) {
				this.dir = dir;
			} else if (new File(dir, "bin").exists()) {
				this.dir = new File(dir, "bin");
			} else if (dir.listFiles(new FileFilter() {
				@Override
				public boolean accept(File file) {
					return file.getName().equals("java") || file.getName().equals("java.exe");
				}
			}).length > 0) {
				this.dir = dir;
			} else {
				throw new IllegalStateException("Couldn't find java executable inside "+dir);
			}
			
			this.is64Bit = is64Bit;
			
			final String[] arr = versionString.split("[\\._]", 5);
			for (int i = 0; i < 4 && i < arr.length; i++) {
				try {
					version[i] = Integer.valueOf(arr[i]);
				} catch (NumberFormatException e) {
					version[i] = -1;
				}
			}
		}

		@Override
		public int compareTo(JavaRuntimeEntry o) {
			// Java 64 bit is preferred
			if (is64Bit != o.is64Bit) {
				return is64Bit ? 1 : -1;
			}
			
			// Java 8 is preferred
			if (version[0] != o.version[0]) {
				if (version[0] == 8 || version[0] > o.version[0]) {
					return 1;
				} else {
					return -1;
				}
			}
			
			// Higher minor versions are preferred
			for (int i = 1; i < 4; i++) {
				if (version[i] > o.version[i]) {
					return 1;
				} else if (version[i] < o.version[i]) {
					return -1;
				}
			}
			
			return 0;
		}

		@Override
		public String toString() {
			return "JREEntry [dir=" + dir + ", ver=" + Arrays.toString(version) + ", is64Bit=" + is64Bit + "]";
		}

		private static boolean guessIf64Bit(File path) {
			try {
				String programFilesX86 = System.getenv("ProgramFiles(x86)");
				return programFilesX86 == null || !path.getCanonicalPath().startsWith(new File(programFilesX86).getCanonicalPath());
			} catch (IOException ignored) {
				return false;
			}
		}
		
		private static String getVersionString(File dir) {
			try {
				return dir.getName().replaceAll("[^\\d._]", "");
			} catch (Exception e) {
				return "-1";
			}
		}
	}

}
