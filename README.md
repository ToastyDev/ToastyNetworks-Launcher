ToastyNetworks Launcher
================
- Downloads: https://forum.toastynetworks.net/resources/
- News post: https://forum.toastynetworks.net/threads/toasty-launcher-new-hardware-more.5344/
- Forked from https://github.com/SKCraft/Launcher

## License
The launcher is licensed under the GNU Lesser General Public License, version 3.
