/*
 * SKCraft Launcher
 * Copyright (C) 2010-2014 Albert Pham <http://www.sk89q.com> and contributors
 * Please see LICENSE.txt for license information.
 */

package com.skcraft.launcher;

import java.awt.Window;
import java.io.File;
import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.google.common.base.Supplier;
import com.skcraft.launcher.dialog.ConsoleFrame;
import com.skcraft.launcher.launch.JavaRuntimeFinder;
import com.skcraft.launcher.launch.JavaRuntimeFinder.JavaRuntimeEntry;
import com.skcraft.launcher.swing.SwingHelper;

import lombok.extern.java.Log;

@Log
public class FancyLauncher {

    public static void main(final String[] args) {
        Launcher.setupLogger();
        
        JavaRuntimeEntry runtime = JavaRuntimeFinder.findBestJRE();
    	if (runtime != null && (runtime.getVersion()[0] != 8 || !runtime.is64Bit())) {
    		JOptionPane.showMessageDialog(null, "You currently have Java "+runtime.getVersion()[0]+" "+(runtime.is64Bit() ? "(64-bit)" : "(32-bit)")+", but our modpacks only support Java 8 64-bit.\nPlease download and install Java 8 - \"Windows Offline (64-bit)\".\nClick OK to open the Java downloads page.", "Invalid Java version", JOptionPane.ERROR_MESSAGE);
    		SwingHelper.openURL("https://www.java.com/en/download/manual.jsp", null);
    		return;
    	}
    	
        log.log(Level.WARNING, "Runtime implementation version: " + Runtime.class.getPackage().getImplementationVersion());
    	if (new File("simple.txt").exists() || Runtime.class.getPackage().getImplementationVersion() == null) {
    		Launcher.main(args);
    		return;
    	}
    	
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.currentThread().setContextClassLoader(FancyLauncher.class.getClassLoader());
                    UIManager.getLookAndFeelDefaults().put("ClassLoader", FancyLauncher.class.getClassLoader());
                    UIManager.getDefaults().put("SplitPane.border", BorderFactory.createEmptyBorder());
                    JFrame.setDefaultLookAndFeelDecorated(true);
                    JDialog.setDefaultLookAndFeelDecorated(true);
                    System.setProperty("sun.awt.noerasebackground", "true");
                    System.setProperty("substancelaf.windowRoundedCorners", "false");

                    if (!SwingHelper.setLookAndFeel("com.skcraft.launcher.skin.LauncherLookAndFeel")) {
                        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    }
                    
                	Launcher launcher = Launcher.createFromArguments(args);
                    launcher.setMainWindowSupplier(new CustomWindowSupplier(launcher));
                    launcher.showLauncherWindow();
                    ConsoleFrame.init();
                } catch (Throwable t) {
                    log.log(Level.WARNING, "Load failure", t);
                	if (t.getStackTrace()[0].getMethodName() == "getDefaultBackgroundColor") {
                		System.exit(1);
                		return;
                	}
                    
                    final int result = JOptionPane.showConfirmDialog(null, "Uh oh! The launcher couldn't be opened because a problem was encountered. Error: " + t.getMessage() + " - Do you want to try loading the simple interface instead?", "Launcher error", JOptionPane.OK_CANCEL_OPTION);
                    if (result == JOptionPane.OK_OPTION) {
                    	Launcher.main(args);
                    } else {
                    	System.exit(1);
                    }
                }
            }
        });
    }

    private static class CustomWindowSupplier implements Supplier<Window> {

        private final Launcher launcher;

        private CustomWindowSupplier(Launcher launcher) {
            this.launcher = launcher;
        }

        @Override
        public Window get() {
            return new FancyLauncherFrame(launcher);
        }
    }

}
